<?php


return [
    'upload' => [
        'success' => ':name uploaded successfully',
        'error' => [
            'no-file' => 'no file to upload'
        ]
    ]
];
