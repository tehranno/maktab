<?php


return [
    'upload' => [
        'success' => ':name با موفقیت آپلود شد',
        'error' => [
            'no-file' => 'فایلی برای آپلود انتخاب نشده است'
        ]
    ]
];
