<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>@yield('title')</title>
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css')}}">
    <style>
        .rtl {
            direction: rtl;
        }
    </style>
</head>

<body>
    <div class="container">
        @yield('content')
    </div>

    <script src="{{ asset('js/bootstrap.min.js')}}"></script>
</body>

</html>