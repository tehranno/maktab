@extends('layout')


@section('title','upload form')

@section('content')

<form method="post" action="{{ route('upload') }}" enctype="multipart/form-data">
    @csrf
    <div class="form-group">
        <label for="upload-file">file input</label>
        <input type="file" class="form-control-file" id="upload-file" name="file">
    </div>
    <button type="submit" class="btn btn-primary">Upload</button>
</form>

@if(session('file-name'))
<div class="alert alert-success mt-2 rtl" role="alert">
    {{ __('messages.upload.success',['name' => session('file-name')]) }}
</div>
@endif

@if(session('no_file_uploaded_error'))
<div class="alert alert-danger mt-2" role="alert">
    {{ __('messages.upload.error.no-file') }}
</div>
@endif
@endsection()