<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FileController extends Controller
{

    /**
     * show upload form
     */
    public function showUploadForm()
    {
        return view('file.upload');
    }


    /**
     * handle upload file
     * 
     * @param Request $request
     * 
     */
    public function upload(Request $request)
    {
        // check existence of file
        if (!$request->hasFile('file')) {
            $request->session()->flash('no_file_uploaded_error', true);
            return back();
        }

        // save original name of uploaded file in session
        $originalName = $request->file('file')->getClientOriginalName();
        $request->session()->flash('file-name', $originalName);

        // upload file code goes here

        return back();
    }
}
